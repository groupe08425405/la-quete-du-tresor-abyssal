import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body class="ile_interactions">
    <h1> La quête du "Trésor Abyssal" </h1>
    
    <div>
      
    <form method="post" action="voyante.py" class="picture">
        <input type="image" src="Images/Perso/voyante.png" class="picture">
    </form>
    
    <form method="post" action="vieillard.py" class="picture">
        <input type="image" src="Images/Perso/vieux.png" class="picture">
    </form>
    
    <form method="post" action="marchand.py" class="picture">
        <input type="image" src="Images/Perso/marchand.png" class="picture">
    </form>
    
    </div>""")

with open("avancement_story.csv", "r") as lect_csv:
        reader = csv.reader(lect_csv)
        for ligne in reader:
            if ligne[0] == "avancement":
                avancement = ligne[1]

if int(avancement) > 1:
    print("""
    <form method="post" action="carte_first.py">
        <input type="submit" value="Retour à la carte" class="redirection">
    </form>""")


print("""
</body>

</html>""")