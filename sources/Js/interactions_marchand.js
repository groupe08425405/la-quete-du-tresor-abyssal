﻿var textes = [
    "Bonjour jeune homme. <br> ",
    "<span class='dialogue_joueur'>Bonjour marchand, je suis un pirate et je suis envoyer ici par le vieillard. <br>Il m'a dit que je pourrais trouver bateau pour accomplir la quête du Trésor Abyssal.</span>",
    "C'est donc vous qui accomplierez cette quête, dans ce cas je ne peux que vous prêter un de mes bateaux et vous souhaiter bonne chance."
];

var avancement = 0;

function change_txt() {
    document.getElementById("interaction_marchand").innerHTML = textes[avancement];
    button();
    avancement += 1;
}

function button() {
    if ( avancement == 2) {
        document.getElementById("bouton_marchand").innerHTML = "<form method='post' action='auto_save.py'> <input type='text' name='avancement' value='2' class='none'> <input type='text' name='next_page' value='carte_first.py' class='none'> <input type='submit' value='Partir sur le large' class='redirection'> </form>";
    }
    else if (avancement == 0) {
        document.getElementById("bouton_marchand").innerHTML = "<button onclick='change_txt()' class='redirection'>Se présenter et demander le bateaux pour la quête</buttton>"
    }
    else {
        document.getElementById("bouton_marchand").innerHTML = "<button  onclick='change_txt()' class='redirection'> Suivant </button>"
    }
}

change_txt()