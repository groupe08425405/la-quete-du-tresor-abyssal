﻿function random_grill() {
    var grid = document.getElementById('grid');
    for(var i = grid.children.length; i >= 0; i--) {
        grid.appendChild(grid.children[Math.random() * i | 0]);
    }
}

function loadGame() {
    random_grill();
}

window.addEventListener('click', function (e) {

    //On click sur un chiffre
    if(e.target.className === 'item') {
        var emptyItem = document.querySelector('.empty');
        
        if (getDistance(e.target.offsetLeft, e.target.offsetTop, emptyItem.offsetLeft, emptyItem.offsetTop) <= 110)
        {
            //échanger la case vide et le chiffre cliqué
            emptyItem.className = 'item';
            emptyItem.innerText = e.target.innerText;
            e.target.className = 'empty';
            e.target.innerText = '';

            Victory();
        }
    }

});

function getDistance(x1, y1, x2, y2) {
    var a = x1 - x2;
    var b = y1 - y2;
    return Math.sqrt(a * a + b * b);
}

function Victory() {

    var items = document.querySelectorAll('#grid>div');
    var score = 0;
    for(var i = 0; i < items.length; i++) {
        if(items[i].innerText === ('' + (i + 1))) {
            score++;
        }
    }

    if(score >= 15) {
        var victoryItem = document.querySelector('.victory');
        victoryItem.style.display = 'block';

        var gameItem = document.querySelector('.game');
        gameItem.style.display = 'none';
    }
}

/*code repris de la vidéo suivante
https://www.youtube.com/watch?v=TxoMtjwNDDE */