﻿var textes = [
    "Bonjour pirate, bienvenue sur l'île de l'espérance. <br>Ici vivent les seules personnes qui croient encore au Trésor abyssal et attendent la venue de l'élu qui arrivera à découvrir ce fabuleux trésor.",
    "Peut-être seras-tu cet(te) élu(e). </br> <button onclick='change_txt()' class='redirection'> Oui </button> <button onclick='no()' class='redirection'> Non </button>",
    
    //si Oui une 1er fois
    "Ah parfait, dans ce cas vas voir le marchand, il te prêtera un bateau pour que tu puisse accomplir ta quête.",
    "Une fois que tu auras pris le large, prend bien garde aux dangers que cette quête te réserve. Pense bien à regarder dans les moindres recoins et fait preuve d'intelligence dans les différentes énigmes qui te seront présentées.",
    "Maintenant que tu sais quoi faire vas. Et si tu ne sais plus quoi faire vas voir la voyante, elle saura quoi te dire pour te guider.",
    
    //si Non une 1er fois
    'Es-tu sûr ? </br> <form method="post" action="hide_end_game.py"><input type="submit" value="Oui" class="redirection"></form> <button onclick="ok_for_mission()" class="redirection"> Non </button>'
];

var avancement = 0;

function change_txt() {
    document.getElementById("interactions_vieillard").innerHTML = textes[avancement];
    button();
    avancement += 1;
}

function no() {
    avancement = 5;
    document.getElementById("interactions_vieillard").innerHTML = textes[avancement];
    button();
}

function ok_for_mission() {
    avancement = 2;
    change_txt();
}

function button() {
    if ( avancement == 1 || avancement == 5) {
        document.getElementById("bouton_vieillard").innerHTML = '';
    }
    else if ( avancement == 4) {
        document.getElementById("bouton_vieillard").innerHTML = "<form method='post' action='ile_interactions.py'> <input type='submit' value='Aller voir le marchand' class='redirection'> </form>";
    }
    else {
        document.getElementById("bouton_vieillard").innerHTML = "<button  onclick='change_txt()' class='redirection'> Suivant </button>"
    }
}

change_txt()