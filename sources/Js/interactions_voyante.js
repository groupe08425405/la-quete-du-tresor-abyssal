﻿var textes = [
    "J'aurais besoins d'aide pour l'une des îles, pouvez-vous m'aider ?",
    'Oui bien sûr, pour laquelle as-tu besion d\'aide ?<br> <button onclick="ile_sanctuaire()" class="redirection">Pour l\'île du sanctuaire</button>  <button onclick="ile_casse_tete()" class="redirection">Pour l\'île de la clé</button>  <button onclick="ile_fantome()" class="redirection">Pour l\'île fantôme</button>',

    /*Pour l'ile sanctuaire*/
    "A oui je la vois très bien.",
    "Humm..... Sur cette île il te faut utiliser la pierre des anciens qui se trouve sur l'île pour décripter le language de nos anciens. <br> Et ainsi trouver le moyen d'ouvrir la cache du trésor.",

    /*Pour l'ile casse tete*/
    "Oula, pour réussir l'épreuve qui se trouve sur cette île, il te faudra faire preuve de beaucoup d'intelligence.",
    "D'après ce que je vois, il te faudra résoudre un \"taquin\". <br> C'est un casse-tête où le but est de remettre les case dans l'ordre des numéros, dans le sens de la lecture.",

    /*Pour l'ile fantome*/
    "Le pirate fantôme est le seul à pouvoir faire sortir l'île du trésor de la mer. <br> C'est d'ailleur de là que le trésor tiens son nom.",
    "Pour répondre à son énigme, il te faut d'abord déchifrer la pierre des anciens, qui se situ sur l'île du sanctuaire."
];

var avancement = 0;

function change_txt() {
    document.getElementById("interaction_voyante").innerHTML = textes[avancement];
    button();
    avancement += 1;
}

function ile_sanctuaire() {
    avancement = 2;
    change_txt();
}
function ile_casse_tete() {
    avancement = 4;
    change_txt();
}
function ile_fantome() {
    avancement = 6;
    change_txt();
}

function button() {
    if ( avancement == 1) {
        document.getElementById("bouton_voyante").innerHTML = "";
    }
    else if (avancement == 3 || avancement == 5 || avancement == 7) {
        document.getElementById("retour_carte").style.display = "inline-block";
        document.getElementById("bouton_voyante").innerHTML = "";
    }
    else {
        document.getElementById("bouton_voyante").innerHTML = "<button  onclick='change_txt()' class='redirection'> Suivant </button>"
    }
}

function hide_thing() {
    document.getElementById("retour_carte").style.display = "none";
}

change_txt()