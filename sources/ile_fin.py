import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body class="ile_fin">
    <h1> La quête du "Trésor Abyssal" </h1>""")

ile_casse_tete = "0"

with open("avancement_story.csv", "r") as lect_csv:
    reader = csv.reader(lect_csv)
    for ligne in reader:
        if ligne[0] == "ile_casse-tete":
            ile_casse_tete = ligne[1]


if ile_casse_tete == "1":
    print("""
        <form method="post" action="end_game.py">
            <input type="image" src="Images/Objet_deco/tresor_close.png" class="picture">
        </form>""")

else:
    print("""
          <img src="Images/Objet_deco/tresor_close.png" class="picture">
        
        <form method="post" action="carte_first.py">
            <input type="submit" value="Finir le quête" class="redirection">
        </form>""")


print("""
</body>

</html>""")