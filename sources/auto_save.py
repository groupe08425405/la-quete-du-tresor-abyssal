import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")

form = cgi.FieldStorage()

print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> Sauvegarde </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body>""")

def lect_csv(nom_fichier, valeur_chercher):
    with open(nom_fichier, "r") as lect_csv:
        reader = csv.reader(lect_csv)
        for ligne in reader:
            if ligne[0] == valeur_chercher:
                return ligne[1]

def add_csv(nom_fichier, nom_valeur, valeur):
    with open(nom_fichier, "a", newline="") as fichier:
        save = csv.writer(fichier)
        save.writerow([nom_valeur,valeur])


avancement = lect_csv("avancement_story.csv", "avancement")


#********************************************************************
if avancement == "0":
    player_name = form.getvalue("player_name")

    with open("player_data.csv", "w", newline="") as name:
        player = csv.writer(name)
        player.writerow(["player_name", player_name])


#********************************************************************
if avancement == "1":
    avancement = form.getvalue("avancement")
    with open("avancement_story.csv", "w", newline="") as file:
        save = csv.writer(file)
        save.writerow(["avancement", 2])
        

#********************************************************************
if avancement == "2":
    avancement_par_ile = form.getvalue("avancement_de_ile")

    if avancement_par_ile == "ile_casse-tete_check" :
        add_csv("avancement_story.csv", "ile_casse-tete", 1)
    elif avancement_par_ile == "ile_fantome_check" :
        add_csv("avancement_story.csv", "ile_fantome", 1)


#********************************************************************
next_page = form.getvalue("next_page")

print("""
    <p>Vous allez être redirigé</p>
    
    <script>
    setTimeout(function() {
        window.location.href = '""", next_page, """';
    }, 1);
    </script>
</body>

</html>""")