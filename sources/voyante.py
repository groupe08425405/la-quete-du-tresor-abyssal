import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
    <script src="Js/interactions_voyante.js"></script>
</head>

<body>
    <h1> La voyante </h1>
""")

def lect_csv(nom_fichier, valeur_chercher):
    with open(nom_fichier, "r") as lect_csv:
        reader = csv.reader(lect_csv)
        for ligne in reader:
            if ligne[0] == valeur_chercher:
                return ligne[1]

avancement = lect_csv("avancement_story.csv", "avancement")
ile_fantome_chek = lect_csv("avancement_story.csv", "ile_fantome")

if avancement == "1":
    print("""
    <p>Huumm .......... <br>
    Je vois un marchand et un bateau. <br>
    Ainsi qu'une grande aventure qui se prépare.
    </p>
    """)

elif avancement == "2":
    if ile_fantome_chek == "show_ile_fin":
        print("""
        <p>Qu'attend tu donc pour aller chercher le trésors sur l'île !!
        </p>
        """)
    
    else:
        print("""
        <p id="interaction_voyante">Huumm .......... <br>
        Je vois que tu as récupéré le bateau...<br>
        A présent il te faut explorer les îles alentours.
        </p>
    
        <div id="bouton_voyante"> <button onclick="change_txt(), hide_thing()" class="redirection"> Lui demander de l'aide pour l'une des îles </button> </div>
        """)


print("""
    <form method="post" action="ile_interactions.py">
        <input type="submit" value="Retourner accomplir la quête" class="redirection" id="retour_carte">
    </form>
</body>

</html>""")