import cgi
print("Content-type: text/html; charset=UTF_8\n")

print("""
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF_8">
   	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>


<body>

    <h1> Personnalise ton pirate </h1>
    
    <form method="post" action="crea_pirate_h.py">
        <input type="submit" value="Homme">
    </form>
    
    <form method="post" action="crea_pirate_f.py">
        <input type="submit" value="Femme">
    </form>
    
    <form method="post" action="carte_first.py">
        <p>
        <label for="pseudo">Ton nom</label> : <input id="pseudo" name="pseudo" placeholder="Il faut que ça en impose" size="30" maxlenght="25"/> </br>
        La couleur de ton chapeau <input type="color" name="color_hat"> </br>
        La couleur de ton haut <input type="color" name="color_shirt"> </br>
        La couleur de ton pantalon <input type="color" name="color_pant"> </br>
        Veux tu avoir une jambe de bois ? <label for="yes">Oui</label> <input type="radio" name="wood_leg" value="True" id="yes" checked>   <label for="no">Non</label> <input type="radio" name="wood_leg" value="False" id="no"> </br>
        La couleur de tes chaussures <input type="color" name="color_shoes">
        <input type="submit" value="En avant !">
      </p>
    </form>
</body>

</html>""")