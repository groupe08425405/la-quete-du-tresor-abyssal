import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body>
    <h1> Le marchand </h1>
""")

with open("avancement_story.csv", "r") as lect_csv:
        reader = csv.reader(lect_csv)
        for ligne in reader:
            if ligne[0] == "avancement":
                avancement = ligne[1]

if avancement == "1" :
    print(f"""
    <p id="interaction_marchand"></p>

    <div id="bouton_marchand"></div>

    <script src="Js/interactions_marchand.js"></script>
    """)

else :
    print("""
    <p>Je n'ai plus rien qui pourrait te servir désolé.</p>
        
    <form method="post" action="ile_interactions.py">
        <input type="submit" value="Retourner faire la quête" class="redirection">
    </form>
    """)


print("""
</body>

</html>""")