import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
    <link rel="icon" type="image/x-icon" href="Images/Ile/ile_simple.jpg">
</head>

<body>
 
    <h1 class="index"> La quête du "Trésor Abyssal" </h1>

    <form method="post" action="auto_save.py" class="index">
        <label for="player_name">Quel sera votre nom :</label>
        <input type="text" name="player_name" id="player_name" class="redirection" required>

        <input type="text" name="next_page" value="vieillard_first.py" class="none">

        <input type="submit" value="Prêt pour l'aventure" class="index redirection">
    </form>
    
    <button onclick="mute_sound()"></button>
      

</body>

</html>""")

with open("avancement_story.csv", "w", newline="") as save:
    avancement = csv.writer(save)
    avancement.writerow(["avancement",0])