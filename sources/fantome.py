import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body>
    <h1> Le fantôme </h1>""")

mdp = ""
form = cgi.FieldStorage()
mdp = form.getvalue("mdp")
mdp = mdp

with open("avancement_story.csv", "r") as lect_csv:
    reader = csv.reader(lect_csv)
    for ligne in reader:
        if ligne[0] == "ile_fantome":
            ile_fantome = ligne[1]
        else :
            ile_fantome = "0"


if ile_fantome == "0":
    if mdp != "au zenith le tresor sortira des abimes":
        print("""
            <form method="post" action="fantome.py">
                <input type="text" name="mdp" placeholder="Rentré le mot de passe">

                <input type="submit" value="Proposer ma réponse" class="redirection">
            </form>
            """)

else:
    print("""
        <p> Tu as déjà répondu à ma question, tu n'as plus qu'à aller chercher le trésor sur son l'île. </p> 

        <form method="post" action="ile_fantome.py">
            <input type="submit" value="Retourner faire la quête" class="redirection">
        </form>""")


if mdp == "au zenith le tresor sortira des abimes":
        print("""
            <p> Bien joué, tu as su décripter la pierre des anciens </p>
            <form method="post" action="auto_save.py">
                <input type="text" name="avancement_de_ile" value="ile_fantome_check" class="none">
                <input typ="text" name="next_page" value="ile_fantome.py" class="none">

                <input type="submit" value="Retouner faire la quête" class="redirection">
            </form>""")

else:
    if ile_fantome != "1":
        print("""
            <p>N'oubli pas de vérifier l'orthographe et qu'il n'y ai pas d'accents. </p>
        
            <form method="post" action="carte_first.py">
                <input type="submit" value="Retour à la carte" class="redirection">
            </form>""")

print("""
</body>

</html>""")