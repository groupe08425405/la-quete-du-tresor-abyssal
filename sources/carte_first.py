import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")

print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La carte </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body class="ile_sur_carte">
    <h1> Carte </h1>
    
    <div>
      
    <form method="post" action="ile_sanctuaire.py" class="picture ile_sur_carte">
        <input type="image" src="Images/Ile/ile_sanctuaire_with_eau.png" class="picture ile_sur_carte">
    </form>""")
    
with open("avancement_story.csv", "r") as lect_csv:
        reader = csv.reader(lect_csv)
        for ligne in reader:
            if ligne[0] == "ile_fantome":
                ile_fantome = ligne[1]
            else:
                 ile_fantome = 0
    
if ile_fantome == "1" :
    print("""
    <form method="post" action="ile_fin.py" class="picture ile_fin">
        <input type="image" src="Images/Ile/ile_finale_with_eau.png" class="picture ile_fin ile_sur_carte">
    </form>""")
else :
    print("""
    <p class="before_ile_fin ile_sur_carte">Ces eaux paraîssent bizarre</p>""")

    
print("""
    <form method="post" action="ile_casse_tete.py" class="picture ile_sur_carte">
        <input type="image" src="Images/Ile/ile_casse-tete_with_eau.png" class="picture ile_sur_carte">
    </form>
    
    </div>
    
    <div>
          
    <form method="post" action="ile_interactions.py" class="picture ile_sur_carte">
        <input type="image" src="Images/Ile/ile_interactions_with_eau.png" class="picture ile_sur_carte">
    </form>
    
    <img src="Images/Objet_deco/bateau_pirate_with_eau.png" class="bateau ile_sur_carte">
    
    <form method="post" action="ile_fantome.py" class="picture ile_sur_carte">
        <input type="image" src="Images/Ile/ile_fantome_with_eau.png" class="picture ile_sur_carte">
    </form>
    
    </div>
</body>

</html>""")