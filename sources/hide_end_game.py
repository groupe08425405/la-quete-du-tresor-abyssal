import cgi
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body>
	<h1> Bien joué vous-avez fini le jeu </h1>
    
	<p>
    Vous avez réussi à finir le jeu (bien que ce n'est pas la manière la plus intéréssante).
    Si vous souhaitez réessayer de finir le jeu de manière plus divertissante vous pouvez <a href="vieillard_first.py" class="redirection">recommencer ici </a>.
    </p>
</body>

</html>""")