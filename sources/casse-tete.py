import cgi, csv
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>
""")

avancement = "0"

with open("avancement_story.csv", "r") as lect_csv:
    reader = csv.reader(lect_csv)
    for ligne in reader:
        if ligne[0] == "ile_casse-tete":
            avancement = ligne[1]

if avancement == "0":
    print("""
    <script src="Js/taquin.js"></script>
        
    <body onload="loadGame()" class="casse-tete">
    <h1> La quête du "Trésor Abyssal" </h1>

    <div class="game">
    <div id="grid">
        <div class="item">1</div>
        
        <div class="item">2</div>
        
        <div class="item">3</div>
        
        <div class="item">4</div>
        
        <div class="item">5</div>
        
        <div class="item">6</div>
        
        <div class="item">7</div>
        
        <div class="item">8</div>
        
        <div class="item">9</div>
        
        <div class="item">10</div>
        
        <div class="item">11</div>
        
        <div class="item">12</div>
        
        <div class="item">13</div>
        
        <div  class="item">14</div>
        
        <div class="item">15</div>
      
        <div class="empty"></div>
    </div>

    <form method="post" action="ile_casse_tete.py">
        <input type="submit" value="Abandonner" class="redirection">
    </form>
    </div>
    
    
    <div class="victory none">
        <img src="Images/Objet_deco/cle.png">
        <p> Bien joué, tu as réussi à résoudre se casse-tête, en récompense voici la clé du trésor des abimes</p>
        <form method="post" action="auto_save.py">
            <input type="text" name="next_page" value="ile_casse_tete.py" class="none">
            <input type="text" name="avancement_de_ile" value="ile_casse-tete_check" class="none">
      
            <input type="submit" value="Continuer la quête" class="redirection">
        </form>
    </div>""")

else:
    print("""
        <body>
        <h1>La quête du "Trésor Abyssal"</h1>
        <p>Tu as déjà fait ce casse-tête.</p>
        
        <form method="post" action="ile_casse_tete.py">
            <input type="submit" value="Retourner faire la quête" class="redirection">
        </form>
        """)

print("""
    </body>

    </html>""")

#code repris de celui de la vidéo suivante
#https://www.youtube.com/watch?v=TxoMtjwNDDE