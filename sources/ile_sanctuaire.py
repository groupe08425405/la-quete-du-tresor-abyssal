import cgi
print("Content-type: text/html; charset=UTF_8\n")


print("""
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF_8">
	<title> La quête du "Trésor Abyssal" </title>
    <link rel="stylesheet" type="text/css" href="Styles/style.css">
</head>

<body class="ile_sanctuaire">
    <h1> La quête du "Trésor Abyssal" </h1>
    
    <img src="Images/Objet_deco/pierre_anciens.png" class="pierre">
    <img src="Images/Objet_deco/tablette_trad.png" class="tablette">
    
    <footer>
        <form method="post" action="carte_first.py">
            <input type="submit" value="Retour à la carte" class="redirection">
        </form>
    </footer>
</body>

</html>""")